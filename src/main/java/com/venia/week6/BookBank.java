package com.venia.week6;

public class BookBank {
    private String name;
    private double balance;
    BookBank(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public boolean deposite(double money) {
        if (money <= 0) {
            return false;
        }
        balance = balance + money;
        return true;
    }

    public boolean withdraw(double money) {
        if (money <= 0) {
            return false;
        }
        if (money > balance) {
            return false;
        }
        balance = balance - money;
        return true;
    }

    public void print() {
        System.out.println(name + " " + balance);

    }

    public String getName(){
        return name;
    }

    public double getBalance(){
        return balance;
    }


}
