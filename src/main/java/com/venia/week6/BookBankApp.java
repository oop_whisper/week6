package com.venia.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank venita = new BookBank("Venita", 100); // Default Constructor
        venita.print();
        venita.deposite(50);
        venita.print();
        venita.withdraw(50);
        venita.print();

        BookBank prayood = new BookBank("Prayood",1);
        prayood.deposite(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweeet = new BookBank("Praweeet",10);
        praweeet.deposite(10000000);
        praweeet.withdraw(1000000);
        praweeet.print();
    }
}
