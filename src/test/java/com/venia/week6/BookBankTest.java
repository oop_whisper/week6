package com.venia.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldDepositeSuccess() {
        BookBank book = new BookBank("Name",0);
        boolean result = book.deposite(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(),0.00001);

    }

    @Test
    public void shouldDepositeNagative() {
        BookBank book = new BookBank("Name",0);
        boolean result = book.deposite(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(),0.00001);
        
    }

    @Test
    public void shouldWithDrawSuccess() {
        BookBank book = new BookBank("Name",0);
        book.deposite(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(),0.00001);
        
    }

    @Test
    public void shouldWithDrawNegative() {
        BookBank book = new BookBank("Name",0);
        book.deposite(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(),0.00001);
        
    }

    @Test
    public void shouldWithDrawOverBalance() {
        BookBank book = new BookBank("Name",0);
        book.deposite(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(),0.00001);
        
    }

    @Test
    public void shouldWithDraw100Balance100() {
        BookBank book = new BookBank("Name",0);
        book.deposite(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getBalance(),0.00001);
        
    }
}
